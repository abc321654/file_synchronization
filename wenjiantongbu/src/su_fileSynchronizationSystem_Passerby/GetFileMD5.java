package su_fileSynchronizationSystem_Passerby;
//本类主要用于根据路径获得文件的MD5码写进txt文本后，生成list表后
//再与存有已经同步过的文件的MD5的list表比较获得可以同步的文件的路径和MD5
import java.io.BufferedReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

public class GetFileMD5 {
	public GetFileMD5(String path) throws IOException {
		List<PathModel> listOfPathAndMd5 = new ArrayList<>();
		List<PathModel> listOfRecord = new ArrayList<>();
		List<PathModel> listOfNotSend = new ArrayList<>();
		getTxtOfFileMD5(path);
		String str_2 = path.replaceAll("\\\\", "+");
		String path2 = str_2 + "的MD5";
		listOfPathAndMd5 = getListOfPathAndMd5(path2);
		// System.out.println("这是想发送的文件的路径和md5");
		// for (PathModel pf3 : listOfPathAndMd5) {
		// String sstr3 = pf3.getPathAndMd5();
		// System.out.println(sstr3);
		// }
		listOfRecord = getListOfRecord();
		// System.out.println("这是发送过的文件的路径和md5");
		// for (PathModel pf2 : listOfRecord) {
		// String sstr2 = pf2.getPathAndMd5();
		// System.out.println(sstr2);
		// }
		listOfPathAndMd5.removeAll(listOfRecord);
		listOfNotSend.addAll(compareFile(listOfPathAndMd5, listOfRecord));
		// System.out.println("这是能发送的文件的路径和md5");
		// for (PathModel pf : listOfPathAndMd5) {
		// String sstr = pf.getPathAndMd5();
		// System.out.println(sstr);
		// }
		// System.out.println("我去也");

	}

	public static void getTxtOfFileMD5(String path) throws IOException {
		// 要遍历的路径
		File file = new File(path); // 获取其file对象
		if (file.isDirectory()) { // 获得文件夹的MD5码
			try {
				func(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (file.isFile()) { // 获得文件的MD5码
			FileInputStream fis = new FileInputStream(file);
			String md5;

			md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fis));

			IOUtils.closeQuietly(fis);
			String str = file + "+MD5:" + md5;
			String str_2 = path.replaceAll("\\\\", "+");
			// System.out.println(str);
			String path2 = str_2 + "的MD5";
			File file2 = new File(path2);
			try {
				FileWriter outOne;

				outOne = new FileWriter(file2, false);

				BufferedWriter outTwo = new BufferedWriter(outOne);

				outTwo.write(str);

				outTwo.newLine();
				outTwo.close();
				outOne.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

	private static void func(File file, String path) throws IOException {
		// String path = file.getAbsolutePath();
		File[] fs = file.listFiles();// listFiles()方法是返回某个目录下所有文件和目录的绝对路径，返回的是File数组
		for (File f : fs) {
			if (f.isDirectory()) // 若是目录，则递归打印该目录下的文件
				func(f, path);
			if (f.isFile()) // 若是文件，直接打印
			{
				FileInputStream fis = new FileInputStream(f);
				String md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fis));
				IOUtils.closeQuietly(fis);
				String str = file + "+MD5:" + md5;
				String str_2 = path.replaceAll("\\\\", "+");
				// System.out.println(str);
				String path2 = str_2 + "的MD5";
				File file2 = new File(path2);
				try {
					FileWriter outOne = new FileWriter(file2, true);
					BufferedWriter outTwo = new BufferedWriter(outOne);
					outTwo.write(str);
					outTwo.newLine();
					outTwo.close();
					outOne.close();
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		}
	}

	public static List<PathModel> getListOfPathAndMd5(String path) {

		// 获得存放文件的路径和MD5码的list
		List<PathModel> listOfPathAndMd5 = new ArrayList<>();
		File file2 = new File(path);
		try {
			Reader in = new FileReader(file2);
			BufferedReader bufferRead = new BufferedReader(in);
			String str_3 = null;
			while ((str_3 = bufferRead.readLine()) != null) {
				String[] ss = str_3.split("\\+");
				PathModel pathModel = new PathModel(ss[0], ss[1]);
				listOfPathAndMd5.add(pathModel);
			}
			bufferRead.close();

		} catch (IOException e1) {
			System.out.println(e1.toString());
		}
		return listOfPathAndMd5;

	}

	public static List<PathModel> getListOfRecord() {
		// 获得存放已同步过的文件的路径和MD5码的list
		List<PathModel> listOfRecord = new ArrayList<>();
		try {
			File file4 = new File("C:\\Users\\小苏\\git\\LAN-file-synchronization\\已同步文件记录.txt");
			Reader in = new FileReader(file4);
			BufferedReader bufferRead = new BufferedReader(in);
			String str_4 = null;
			while ((str_4 = bufferRead.readLine()) != null) {
				String[] ss = str_4.split("\\+");
				PathModel pathModel = new PathModel(ss[0], ss[1]);
				listOfRecord.add(pathModel);
			}
			bufferRead.close();
		} catch (IOException e1) {
			System.out.println(e1.toString());
		}
		return listOfRecord;
	}

	public static List<PathModel> compareFile(List<PathModel> pathModel1, List<PathModel> pathModel2) {
		List<PathModel> list = new ArrayList<PathModel>();
		if (pathModel2.size() == 0) {
			list.addAll(pathModel1);
		} else {
			for (PathModel pm : pathModel1) {
				if (pathModel2.contains(pm)) {
					pathModel1.remove(pm);
				}
			}
			list.addAll(pathModel1);
		}
		return list;
	}
}
