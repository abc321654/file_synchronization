package su_fileSynchronizationSystem_Passerby;

public class PathModel {
	public String path;
	public String md5;

	public PathModel(String path, String md5) {
		this.path = path;
		this.md5 = md5;
	}

	public String getPath() {
		return path;
	}

	public void setFile(String path) {
		this.path = path;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public String getPathAndMd5() {
		String str = path + "+" + md5;
		return str;
	}

	public boolean equals(Object obj) {
		PathModel pm = (PathModel) obj;
//		if (!path.equals(pm.path)) {
//			return false;
//		}
			
		return md5.equals(pm.md5);
	}

}
