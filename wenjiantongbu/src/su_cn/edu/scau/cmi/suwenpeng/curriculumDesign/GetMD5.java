package su_cn.edu.scau.cmi.suwenpeng.curriculumDesign;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

public class GetMD5 {
	public static void main(String[] args) throws IOException {
		String path = "C:/Users/小苏/Documents/Tencent Files";		//要遍历的路径
		File file = new File(path);		//获取其file对象
		func(file);
	}
	
	private static void func(File file) throws IOException{
		File[] fs = file.listFiles();
		for(File f:fs){
			if(f.isDirectory())	//若是目录，则递归打印该目录下的文件
				func(f);
			if(f.isFile())		//若是文件，直接打印
				{
				FileInputStream fis= new FileInputStream(f); 
				String md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fis));   
		        IOUtils.closeQuietly(fis);   
		        System.out.println(f+" MD5:"+md5);
				}
		}
	}
}
