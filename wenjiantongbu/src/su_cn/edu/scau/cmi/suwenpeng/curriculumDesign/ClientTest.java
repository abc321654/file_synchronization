package su_cn.edu.scau.cmi.suwenpeng.curriculumDesign;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class ClientTest {

	public static void main(String[] args) {
		Socket socket = null;
        OutputStream os = null;
        FileInputStream fis = null;
        InputStream is = null;
        try {
            //创建一个Socket的对象
            socket = new Socket(InetAddress.getByName("172.16.85.206"),9090);
            //从本地获取一个文件发送给服务端
            os = socket.getOutputStream();
            fis = new FileInputStream(new File("C:\\Users\\小苏\\Desktop\\软件工程专业就业\\生涯发展.docx"));
            byte[] b = new byte[1024];
            int length;
            while((length = fis.read(b)) != -1){
                os.write(b,0,length);
            }
            socket.shutdownOutput();
            //接收来自服务端的信息
            is = socket.getInputStream();
            byte[] b1 = new byte[1024];
            int length1;
            while((length1 = is.read(b1)) != -1){
                String str = new String(b1,0,length1);
                System.out.println(str);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        finally{
            //关闭相应的流和Socket对象
            if(is != null){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(os != null){
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(socket != null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}

}
