package su_cn.edu.scau.cmi.suwenpeng.curriculumDesign;

import java.io.*;
import java.net.Socket;

public class ReceiveFile {
	protected void receiveFile(Socket socket) {
		String mFilePath = null;
		File dirs = new File(mFilePath);
		if (!dirs.exists()) {
			dirs.mkdirs();
		}
		DataInputStream din = null;
		int fileNum = 0;
		long totalSize = 0;
		FileInfo[] fileinfos = null;
		try {
			din = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
			fileNum = din.readInt();
			fileinfos = new FileInfo[fileNum];
			for (int i = 0; i < fileNum; i++) {
				fileinfos[i] = new FileInfo();
				fileinfos[i].mFileName = din.readUTF();
				fileinfos[i].mFileSize = din.readLong();
			}
			totalSize = din.readLong();
		} catch (IOException e) {
			e.printStackTrace();
			//Log.d(TAG, "readInt Exception");
			System.exit(0);
		}
		System.out.println(fileNum);
		System.out.println(totalSize);
		for (FileInfo fileinfo : fileinfos) {
			System.out.println(fileinfo.mFileName);
			System.out.println(fileinfo.mFileSize);
		}
		// // /////////////////////////////////////////////////////////////////
		int leftLen = 0; // 写满文件后缓存区中剩余的字节长度。

		int bufferedLen = 0; // 当前缓冲区中的字节数
		int writeLen = 0; // 每次向文件里写入的字节数
		long writeLens = 0; // 当前已经向单个文件里写入的字节总数
		long totalWriteLens = 0; // 写入的所有字节数
		byte buf[] = new byte[8192];
		for (int i = 0; i < fileNum; i++) {
			writeLens = 0;
			try {
				FileOutputStream fout = new FileOutputStream(mFilePath + fileinfos[i].mFileName);
				while (true) {
					if (leftLen > 0) {
						bufferedLen = leftLen;
					} else {
						bufferedLen = din.read(buf);
					}
					if (bufferedLen == -1)
						return;
					System.out.println("readlen" + bufferedLen);
					// 假设已写入文件的字节数加上缓存区中的字节数已大于文件的大小，仅仅写入缓存区的部分内容。
					if (writeLens + bufferedLen >= fileinfos[i].mFileSize) {
						leftLen = (int) (writeLens + bufferedLen - fileinfos[i].mFileSize);
						writeLen = bufferedLen - leftLen;
						fout.write(buf, 0, writeLen); // 写入部分
						totalWriteLens += writeLen;
						move(buf, writeLen, leftLen);
						break;
					} else {
						fout.write(buf, 0, bufferedLen); // 所有写入
						writeLens += bufferedLen;
						totalWriteLens += bufferedLen;
						if (totalWriteLens >= totalSize) {
							// mListener.report(GroupChatActivity.FAIL, null);
							return;
						}
						leftLen = 0;
					}
					// mListener.report(GroupChatActivity.PROGRESS,
					// (int) (totalWriteLens * 100 / totalSize));
				} // end while
				fout.close();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//Log.d(TAG, "receive file Exception");
			}
		} // end for
			// mListener.report(GroupChatActivity.FAIL, null);
	}

	private void move(byte[] buf, int writeLen, int leftLen) {
		// TODO Auto-generated method stub
		
	}

}
