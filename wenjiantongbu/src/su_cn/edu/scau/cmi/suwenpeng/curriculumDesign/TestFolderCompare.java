package su_cn.edu.scau.cmi.suwenpeng.curriculumDesign;

import java.io.*;

import java.util.*;
import org.apache.commons.codec.digest.DigestUtils; 
public class TestFolderCompare {
	
	    /**
	     * 根据路径获取所有的文件夹和文件,及文件的md5值
	     * @param path 路径
	     */
	    private static Map<String, FileModel> getFiles(String path) throws IOException {
	        Map<String, FileModel> map = new HashMap<String, FileModel>();
	        File folder = new File(path);
	        Object[] files = getFileList(folder).toArray();
	        Arrays.sort(files);
	        for (Object obj : files) {
	            File file = (File) obj;
	            // 去掉根目录,正则的\\\\,转义为java的\\,再转义为\
	            String key = file.getAbsolutePath().replaceAll("\\\\", "/").replaceAll(path, "");
	            String md5 = "";// 文件夹不比较md5值
	            if (file.isFile()) {
	                md5 = DigestUtils.md5Hex(new FileInputStream(file));//DigestUtils实现md5码加密
	            }
	            FileModel fileModel = new FileModel(file, md5);
	            map.put(key, fileModel);
	        }
	        return map;
	    }

	    /**
	     * 递归获取路径下所有文件夹和文件
	     * @param folder 文件路径
	     */
	    private static List<File> getFileList(File folder) {
	        List<File> list = new ArrayList<File>();
	        File[] files = folder.listFiles();
	        for (File file : files) {
	            list.add(file);
	            if (file.isDirectory()) {
	                List<File> fileList = getFileList(file);
	                list.addAll(fileList);
	            }
	        }
	        return list;
	    }

	    /**
	     * 比较两个文件集合的不同
	     * @param fileMap1 文件集合
	     * @param fileMap2 文件集合
	     */
	    public static List<FileModel> compareFile(Map<String, FileModel> fileMap1, Map<String, FileModel> fileMap2) {
	        List<FileModel> list = new ArrayList<FileModel>();
	        for (String key : fileMap1.keySet()) {
	            FileModel fileModel1 = fileMap1.get(key);
	            FileModel fileModel2 = fileMap2.get(key);
	            // 将fileMap2中没有的文件夹和文件,添加到结果集中
	            if (fileModel2 == null) {
	                list.add(fileModel1);
	                continue;
	            }
	            // 文件的md5值不同则add到比较结果集中
	            if (fileModel1.getFile().isFile() && !fileModel1.getMd5().equals(fileModel2.getMd5())) {
	                list.add(fileModel1);
	            }
	        }
	        return list;
	    }

	    public static void main(String[] args) throws IOException {
	        String path1 = "C:/Users/小苏/Desktop/勤工申请";
	        String path2 = "C:/Users/小苏/Documents/Tencent Files";
	        // 获取路径下所有文件夹和文件,及文件的md5值
	        Map<String, FileModel> fileMap1 = getFiles(path1);
	        Map<String, FileModel> fileMap2 = getFiles(path2);
	        List<FileModel> resultList = new ArrayList<FileModel>();
	        // 得到fileMap2中没有的文件夹和文件,及md5值不同的文件
	        resultList.addAll(compareFile(fileMap1, fileMap2));
	        // 得到fileMap1中没有的文件夹和文件,及md5值不同的文件
	        resultList.addAll(compareFile(fileMap2, fileMap1));
	        // 输出最终结果
	        for (FileModel fileModel : resultList) {
	            System.out.println(fileModel.getFile().getAbsolutePath() + " " + fileModel.getMd5());
	        }
	    }
	
}
