package su_cn.edu.scau.cmi.suwenpeng.curriculumDesign;
//放弃使用
import java.io.*;
import java.net.Socket;

public class SocketClient {

	public static void main(String[] args) throws Exception {
	    // 要连接的服务端IP地址和端口
	    String host = "172.16.85.206";
	    int port = 9090;
	    // 与服务端建立连接
	    Socket socket = new Socket(host, port);
	    // 建立连接后获得输出流
	    OutputStream outputStream = socket.getOutputStream();
	    String message = "你好  yiwangzhibujian";
	    socket.getOutputStream().write(message.getBytes("UTF-8"));
	    //通过shutdownOutput高速服务器已经发送完数据，后续只能接受数据
	    socket.shutdownOutput();
	    
	    InputStream inputStream = socket.getInputStream();
	    byte[] bytes = new byte[1024];
	    int len;
	    StringBuilder sb = new StringBuilder();
	    while ((len = inputStream.read(bytes)) != -1) {
	      //注意指定编码格式，发送方和接收方一定要统一，建议使用UTF-8
	      sb.append(new String(bytes, 0, len,"UTF-8"));
	    }
	    System.out.println("get message from server: " + sb);
	    
	    inputStream.close();
	    outputStream.close();
	    socket.close();
	  }

}
