package su_cn.edu.scau.cmi.suwenpeng.curriculumDesign;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

public class GetAddress {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			InetAddress address=InetAddress.getLocalHost();
			System.out.println("计算机名："+address.getHostName());
			System.out.println("IP地址："+address.getHostAddress());
			byte[] bytes=address.getAddress();
			System.out.println( "字节形式的数组IP："+Arrays.toString(bytes));
			System.out.println(address);
			
			
		}
		catch(UnknownHostException e) {
			e.printStackTrace();
		} 
	}

}
