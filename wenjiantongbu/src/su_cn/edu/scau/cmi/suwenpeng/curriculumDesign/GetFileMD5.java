package su_cn.edu.scau.cmi.suwenpeng.curriculumDesign;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

//获得文件或者文件夹的MD5码
public class GetFileMD5 {

	public static void main(String[] args) throws IOException {
		String path = "C:\\Users\\小苏\\Documents\\Tencent Files\\1770527753"; // 要遍历的路径
		String str = null;
		File file = new File(path); // 获取其file对象
		if (file.isDirectory()) { // 获得文件夹的MD5码
			func(file);
		}
		if (file.isFile()) { // 获得文件的MD5码
			FileInputStream fis = new FileInputStream(file);
			String md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fis));
			IOUtils.closeQuietly(fis);
			str = file + " MD5:" + md5;
//			 System.out.println(str);
			 String path2 = "C:+Users+小苏+Documents+Tencent Files+1770527753的MD5";
			File file2 = new File(path2);//"的MD5"
			try {
				FileWriter outOne = new FileWriter(file2,true);
				BufferedWriter outTwo = new BufferedWriter(outOne);
				outTwo.write(str);
				outTwo.newLine();
				outTwo.close();
				outOne.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}

	}

	private static void func(File file) throws IOException {
		String path = file.getAbsolutePath();
		File[] fs = file.listFiles();// listFiles()方法是返回某个目录下所有文件和目录的绝对路径，返回的是File数组
		for (File f : fs) {
			if (f.isDirectory()) // 若是目录，则递归打印该目录下的文件
				func(f);
			if (f.isFile()) // 若是文件，直接打印
			{
				FileInputStream fis = new FileInputStream(f);
				String md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fis));
				IOUtils.closeQuietly(fis);
				String str = file + " MD5:" + md5;
//				 System.out.println(str);
				 String path2 = "C:+Users+小苏+Documents+Tencent Files+1770527753的MD5";
				File file2 = new File(path2);
				try {
					FileWriter outOne = new FileWriter(file2,true);
					BufferedWriter outTwo = new BufferedWriter(outOne);
					outTwo.write(str);
					outTwo.newLine();
					outTwo.close();
					outOne.close();
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		}
	}

}
