package deletePhoto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class GetDifferentMD5 {

	public static void main(String[] args) throws IOException {
		String path_1 = "E:\\MD5.txt";// Client传送过来的MD5的txt文本路径
		String path_2 = "E:\\MD5second.txt";// Server文件的文件夹路径
		List<PathModel> listOfClient = new ArrayList<>();
		List<PathModel> listOfServer = new ArrayList<>();
		listOfClient = getListOfPathAndMd5(path_1);// 从txt获得客户端发送过来的文件夹的MD5的list
		listOfServer = getListOfPathAndMd5(path_2);// 从txt获得服务端这边的文件夹的MD5的list
		listOfClient.removeAll(listOfServer);
	}

	public static List<PathModel> getListOfPathAndMd5(String path) {

		// 获得存放文件的路径和MD5码的list
		List<PathModel> listOfPathAndMd5 = new ArrayList<>();
		File file = new File(path);
		try {
			Reader in = new FileReader(file);
			BufferedReader bufferRead = new BufferedReader(in);
			String str = null;
			while ((str = bufferRead.readLine()) != null) {
				String[] ss = str.split("\\+");
				PathModel pathModel = new PathModel(ss[0], ss[1]);
				listOfPathAndMd5.add(pathModel);
			}
			bufferRead.close();

		} catch (IOException e1) {
			System.out.println(e1.toString());
		}
		return listOfPathAndMd5;

	}

}
