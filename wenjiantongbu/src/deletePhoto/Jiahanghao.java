package deletePhoto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Jiahanghao {
	/*
	 * 将day19-笔记.txt中每一行前面加上行号和冒号
	 */
	public static void main(String args[]) {
		// 因为考虑到每行数据，用BufferedReader 比较方便
		BufferedReader br = null;
		BufferedWriter bw = null;
		try {
			br = new BufferedReader(new FileReader("‪F:\\微博图片2018_10_19的MD5.txt"));
			bw = new BufferedWriter(new FileWriter("‪‪F:\\微博图片2018_10_19的MD5=1.txt"));
			String lineString = null;
			int i = 0;
			while (true) {
				i++;
				// BufferedReader 的readline 方法，直接读取一行数据
				lineString = br.readLine();
				if (lineString == null) {
					break;
				} else {
					lineString = "第" + i + "行：" + lineString;
					bw.write(lineString); // 写入一行数据
					bw.newLine(); // 另起一行
					System.out.println(lineString);
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {

				br.close();
				bw.close();
			} catch (IOException e) {
				System.out.println(e);
			}
		}
	}
}
