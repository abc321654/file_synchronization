package work;

import java.io.IOException;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import work.model.Address;
import work.model._File;
import work.view.AddressEditDialogController;
import work.view.ChoosePageController;
import work.view.OverviewController;
import work.view.RootLayoutController;

public class RootLayout extends Application {

    private Stage primaryStage = new Stage();
    private AnchorPane rootLayout;

    private ObservableList<_File>fileData = FXCollections.observableArrayList();  // 一个可观察的文件的列表数据

    public ObservableList<_File>getFileData(){
    	return fileData;
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception{
    	
    	//Parent root = FXMLLoader.load(getClass().getResource("RootLayout.fxml"));
    	
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Send Mode");

        //窗口左上角的图标
//        this.primaryStage.getIcons().add(new Image("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=928320189,1785427405&fm=15&gp=0.jpg"));
        
        initRootLayout();

        //showOverview();
    }
    
    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = (AnchorPane) loader.load();
            
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            
            // Give the controller access to the main app.
            RootLayoutController controller = loader.getController();
            controller.setRootLayout(this);
            
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the person overview inside the root layout.
     */
    /**
     * Shows the person overview inside the root layout.
     */
    public void showFileOverview() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            AnchorPane Overview = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.

            // Give the controller access to the main app.
            RootLayoutController controller = loader.getController();
            
            controller.setRootLayout(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
    
    public boolean showAddressEditDialog(Address address) {   // 显示编辑IP地址的窗口
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/AddressEditDialog.fxml"));
			AnchorPane page = (AnchorPane)loader.load();
			
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edit Address");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			
			AddressEditDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setAddress(address);
			
			dialogStage.showAndWait();
			
			return controller.isOkClicked();
		}
		catch(IOException e) {
			e.printStackTrace();
			return false;
		}
	}
    
    public void  showWindow() throws Exception {
		start(this.primaryStage);
	}

    
}