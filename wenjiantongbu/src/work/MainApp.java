package work;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lan.SendMD5;
import work.view.ChoosePageController;

public class MainApp extends Application {

	private Stage primaryStage;
	private AnchorPane choosePage;
	private SendMD5 now;

	public MainApp() {

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// now = new SendMD5();
		// now.sendMain();
		// Parent root =
		// FXMLLoader.load(MainApp.class.getResource("view/ChoosePage.fxml"));

		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("File synchronization");

		// �������Ͻǵ�ͼ��
		// this.primaryStage.getIcons().add(new
		// Image("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=928320189,1785427405&fm=15&gp=0.jpg"));

		initChoosePage();

		// showOverview();
	}

	/**
	 * Initializes the root layout.
	 */
	public void initChoosePage() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/ChoosePage.fxml"));
			choosePage = (AnchorPane) loader.load();

			// Give the controller access to the main app.
			ChoosePageController controller = loader.getController();
			controller.setMainApp(this);

			// Show the scene containing the root layout.
			Scene scene = new Scene(choosePage);
			primaryStage.setScene(scene);

			primaryStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Shows the person overview inside the root layout.
	 */
	/**
	 * Shows the person overview inside the root layout.
	 */
	// public void showOverview() {
	// try {
	// // Load person overview.
	// FXMLLoader loader = new FXMLLoader();
	// loader.setLocation(MainApp.class.getResource("view/Overview.fxml"));
	// AnchorPane Overview = (AnchorPane) loader.load();
	//
	// // Set person overview into the center of root layout.
	//
	// // Give the controller access to the main app.
	// OverviewController controller = loader.getController();
	// controller.setMainApp(this);
	//
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * Returns the main stage.
	 * 
	 * @return
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public static void main(String[] args) {
		launch(args);
	}
}