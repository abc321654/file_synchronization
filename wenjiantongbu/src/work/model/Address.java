package work.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Address {
	
	private final StringProperty ip;
	private final StringProperty port;
	
	public Address() {
		this(null,null);
	}
	
	public Address(String ip,String port) {
		this.ip = new SimpleStringProperty(ip);
		this.port = new SimpleStringProperty(port);
	}
	
	public String getIp() {
		return ip.get();
	}
	
	public void setIp(String ip) {
		this.ip.set(ip);
	}
	
	public StringProperty ipProperty() {
		return ip;
	}
	
	public String getPort() {
		return port.get();
	}
	
	public void setPort(String port) {
		this.port.set(port);
	}
	
	public StringProperty portProperty() {
		return port;
	}
	
}
