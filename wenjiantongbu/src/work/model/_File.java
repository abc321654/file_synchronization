package work.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class _File {
	
	private final StringProperty name;
	private final StringProperty path;
	
	public _File() {
		this(null,null);
	}
	
	public _File(String name,String path) {
		this.name = new SimpleStringProperty(name);
		this.path = new SimpleStringProperty(path);
	}
	
	public String getName() {
		return name.get();
	}
	
	public void setName(String name) {
		this.name.set(name);
	}
	
	public StringProperty nameProperty() {
		return name;
	}
	
	public String getPath() {
		return path.get();
	}
	
	public void setPath(String path) {
		this.path.set(path);
	}
	
	public StringProperty pathProperty() {
		return path;
	}
	
}
