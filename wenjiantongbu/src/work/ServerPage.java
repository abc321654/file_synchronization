package work;

import java.io.IOException;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import work.model.Address;
import work.model._File;
import work.view.AddressEditDialogController;
import work.view.OverviewController;
import work.view.ServerPageController;

public class ServerPage extends Application {

    private Stage primaryStage = new Stage();
    private AnchorPane serverPage;

    private ObservableList<_File>fileData = FXCollections.observableArrayList();  // 一个可观察的文件的列表数据

    public ObservableList<_File>getFileData(){
    	return fileData;
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception{
    	
    	//Parent root = FXMLLoader.load(getClass().getResource("RootLayout.fxml"));
    	
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Receive Mode");

        //窗口左上角的图标
//        this.primaryStage.getIcons().add(new Image("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=928320189,1785427405&fm=15&gp=0.jpg"));
        
        initServerPage();

        //showOverview();
    }
    
    /**
     * Initializes the root layout.
     */
    public void initServerPage() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ServerPage.fxml"));
            serverPage = (AnchorPane) loader.load();
            
            // Show the scene containing the root layout.
            Scene scene = new Scene(serverPage);
            
//            ToggleGroup group = new ToggleGroup();
//            RadioButton button1 = new RadioButton("select first");
//            button1.setToggleGroup(group);
//            button1.setSelected(true);
//            RadioButton button2 = new RadioButton("select second");
//            button2.setToggleGroup(group);
//            
//            serverPage.getChildren().add(button1);
//            serverPage.getChildren().add(button2);

            
            
            primaryStage.setScene(scene);
            
            // Give the controller access to the main app.
            ServerPageController controller = loader.getController();
            controller.setServerPage(this);
            
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the person overview inside the root layout.
     */
    /**
     * Shows the person overview inside the root layout.
     */
    public void showOverview() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/Overview.fxml"));
            AnchorPane Overview = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.

            // Give the controller access to the main app.
            OverviewController controller = loader.getController();
            //controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
    
    //增加IP输入的编辑框
    public boolean showAddressEditDialog(Address address) {   // 显示编辑成绩的窗口
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/AddressEditDialog.fxml"));
			AnchorPane page = (AnchorPane)loader.load();
			
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edit Address");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			
			AddressEditDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setAddress(address);
			
			dialogStage.showAndWait();
			
			return controller.isOkClicked();
		}
		catch(IOException e) {
			e.printStackTrace();
			return false;
		}
	}
    
    public void  showWindow() throws Exception {
		start(this.primaryStage);
	}

    
}