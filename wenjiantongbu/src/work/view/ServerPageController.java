package work.view;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import lan.GetModel;
import lan.MyServer;
import lan.Server2;
import org.controlsfx.dialog.Dialogs;

import su_holsonWay.Server;
import work.MainApp;
import work.RootLayout;
import work.ServerPage;
import work.model.Address;
import work.model._File;

/**
 * The controller for the root layout. The root layout provides the basic
 * application layout containing a menu bar and space where other JavaFX
 * elements can be placed.
 * 
 * @author Marco Jakob
 */
public class ServerPageController {
	
	//Stage stage=new Stage();
	 
	@FXML
	private TableView<_File>fileTable;
	@FXML
	private TableColumn<_File,String>nameColumn;
	@FXML
	private TableColumn<_File,String>pathColumn;
	
	private ServerPage serverPage;
	private Address address;
	private Server2 server2;
	private GetModel getModel;
	private MyServer myServer;
	private int siz = -1;

	//定义需要同步的文件
	public HashMap<Integer, String> chooseFile = new HashMap<Integer, String>();
	
	public ServerPageController() {
		
	}

//	public void start(Stage primaryStage) throws Exception{
//        Parent root = FXMLLoader.load(getClass().getResource("RootLayout.fxml"));
//        //primaryStage.setTitle("Hello World");
//        //primaryStage.setScene(new Scene(root, 600, 400));
//        primaryStage.show(); 
//    }
	
	
	@FXML
	private void initialize() {
		nameColumn.setCellValueFactory(cellData->cellData.getValue().nameProperty());
	}
	
	@FXML
    private void handleLink() {
    	
    	address = new Address();
    	serverPage.showAddressEditDialog(address);

    }
	
	@FXML
    private void handleOpenFile() throws IOException {
        
    	FileInputStream fin = null;
    	DataOutputStream dout = null;
    	int length = 0;

		DirectoryChooser directoryChooser = new DirectoryChooser();

		File file = directoryChooser.showDialog(serverPage.getPrimaryStage());

		_File tempFile = new _File();

    	tempFile.setName(file.getName());
    	tempFile.setPath(file.getAbsolutePath());

		if (!chooseFile.containsValue(tempFile.getPath())) {
			siz++;
			serverPage.getFileData().add(tempFile);
			chooseFile.put(siz, tempFile.getPath());
		}

    }
    
    public void handleDeleteFile() {
    	int selectedIndex = fileTable.getSelectionModel().getSelectedIndex();
    	if(selectedIndex>=0) {
    		fileTable.getItems().remove(selectedIndex);
    		chooseFile.clear();
    		List<_File>file;
    		file = serverPage.getFileData();
			siz = -1;
			for(_File temp:file) {
				siz++;
				chooseFile.put(siz, temp.getPath());
			}
    	}
    }

    public void letsGo() {
		getModel = new GetModel();

		int now = siz;

		int model = getModel.getModel(8888);
		if (model == 3) {
			myServer = new MyServer(33333);
		} else
			server2 = new Server2(33333, 55522);
		System.out.println(model);
		for (String it : chooseFile.values()) {
			System.out.println(it);
			if (model == 3) {
				if (!myServer.ServerStart(it))
					break;
			} else {
				try {
					System.out.println("要开始了");
					server2.solve(it, model);
				} catch (Exception e) {
					break;
				}
			}
			fileTable.getItems().remove(0);
		}
		chooseFile.clear();
		List<_File>file;
		file = serverPage.getFileData();
		siz = -1;
		for(_File temp:file) {
			siz++;
			chooseFile.put(siz, temp.getPath());
		}
		if (model == 3) {
			myServer.clr();
		} else
			server2.clr();

		getModel.clr();
		            //完成同步时，弹出成功提示
            Alert alert = new Alert(AlertType.INFORMATION);  // 完成同步时，弹出成功提示
        	alert.setTitle("Finished");
        	alert.setHeaderText("Successful synchronization, synchronize " + (now - siz));
        	alert.setContentText("Congratulations");
        	alert.showAndWait();
	}

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param
     */
    public void setServerPage(ServerPage serverPage) {
        this.serverPage = serverPage;
        
        fileTable.setItems(serverPage.getFileData());
    }


}