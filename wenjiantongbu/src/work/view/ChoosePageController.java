package work.view;

import javafx.fxml.FXML;
import work.MainApp;
import work.RootLayout;
import work.ServerPage;

public class ChoosePageController {
	
	private MainApp mainApp;
	
	public ChoosePageController(){
		
	}
	
	@FXML
	private void initialize() {
		
	}
	
	
	public void changeWindow() throws Exception {
		work.RootLayout rootLayout = new RootLayout();
		rootLayout.showWindow();
		//mainApp.getPrimaryStage().close();
	}

	public void changeWindow2() throws Exception{
		work.ServerPage serverPage = new ServerPage();
		serverPage.showWindow();
		//mainApp.getPrimaryStage().close();
	}
	
	public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
	
	
}
