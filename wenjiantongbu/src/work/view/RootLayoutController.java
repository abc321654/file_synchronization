package work.view;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
//import java.awt.List;
import java.util.HashMap;
import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.stage.DirectoryChooser;
import lan.MyClient;
import lan.SendMD5;
import lan.SendModel;
import work.RootLayout;
import work.model.Address;
import work.model._File;

/**
 * The controller for the root layout. The root layout provides the basic
 * application layout containing a menu bar and space where other JavaFX
 * elements can be placed.
 *
 * @author Marco Jakob
 */
public class RootLayoutController {

	// Stage stage=new Stage();

	@FXML
	private TableView<_File> fileTable;
	@FXML
	private TableColumn<_File, String> nameColumn;
	@FXML
	private TableColumn<_File, String> pathColumn;
	@FXML
	private RadioButton fullAdd;
	@FXML
	private RadioButton add;
	@FXML
	private RadioButton mirror;

	final ToggleGroup group = new ToggleGroup();

	private RootLayout rootLayout;
	private Address address;
	private SendMD5 solve = new SendMD5();
	private MyClient myClient = new MyClient();
	private SendModel sendModel = new SendModel();
	private int siz = -1;

	// public List<String>sendFile = new ArrayList<String>();
	// HashMap:是基于哈希表的Map接口实现。哈希表的作用是用来保证键的唯一性的。
	// HashMap<String,String>，键：String，值：String
	public HashMap<Integer, String> sendFile = new HashMap<Integer, String>();

	public RootLayoutController() {

	}

	@FXML
	private void initialize() {
		nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
		fullAdd.setToggleGroup(group);
		add.setToggleGroup(group);
		mirror.setToggleGroup(group);
	}

	/**
	 * Is called by the main application to give a reference back to itself.
	 *
	 * @param mainApp
	 */
	// public void setRootLayout(RootLayout rootLayout) {
	// this.rootLayout = rootLayout;
	// }

	/**
	 * Creates an empty address book.
	 */
	// @FXML
	// private void handleNew() {
	// mainApp.getPersonData().clear();
	// mainApp.setPersonFilePath(null);
	// }

	/**
	 * Opens a FileChooser to let the user select an address book to load.
	 * 
	 * @throws IOException
	 */

	@FXML
	private void handleLink() {

		address = new Address();
		rootLayout.showAddressEditDialog(address);
	}

	@FXML
	private void handleOpenFile() throws IOException {

		FileInputStream fin = null;
		DataOutputStream dout = null;
		int length = 0;

		DirectoryChooser directoryChooser = new DirectoryChooser();

		File file = directoryChooser.showDialog(rootLayout.getPrimaryStage());

		_File tempFile = new _File();

		tempFile.setName(file.getName());
		tempFile.setPath(file.getAbsolutePath());

		if (!sendFile.containsValue(tempFile.getPath())) {
			siz++;
			rootLayout.getFileData().add(tempFile);
			sendFile.put(siz, tempFile.getPath());
		}

	}

	public void handleDeleteFile() {
		int selectedIndex = fileTable.getSelectionModel().getSelectedIndex();
		System.out.println(selectedIndex);
		if (selectedIndex >= 0) {
			fileTable.getItems().remove(selectedIndex);
			sendFile.clear();
			List<_File> file;
			file = rootLayout.getFileData();
			siz = -1;
			for (_File temp : file) {
				siz++;
				sendFile.put(siz, temp.getPath());
			}
		}
	}

	public void letsGo() {
		int model = 0;
		if (fullAdd.isSelected())
			model = 1;
		else if (add.isSelected())
			model = 2;
		else if (mirror.isSelected())
			model = 3;
		else {
			Alert alert = new Alert(Alert.AlertType.WARNING); // 没有选中时警告
			alert.setTitle("Warning");
			alert.setHeaderText("_(:з)∠)_");
			alert.setContentText("Please select exactly one model!");
			alert.showAndWait();
			return;
		}

		int now = siz;

		sendModel.sendModel(address.getIp(), 8888, model);

		if (model != 3)
			solve.setHost(address.getIp());
		for (String it : sendFile.values()) {
			if (model == 3) {
				if (!myClient.ClientStart(it, address.getIp(), 33333))
					break;
			} else if (!solve.sendMain(it, model))
				break;
			fileTable.getItems().remove(0);
		}
		// 完成同步时，弹出成功提示
		sendFile.clear();
		List<_File> file;
		file = rootLayout.getFileData();
		siz = -1;
		for (_File temp : file) {
			siz++;
			sendFile.put(siz, temp.getPath());
		}
		Alert alert = new Alert(Alert.AlertType.INFORMATION); // 完成同步时，弹出成功提示
		alert.setTitle("Finished");
		alert.setHeaderText("Successful synchronization, synchronize " + (now - siz));
		alert.setContentText("Congratulations");
		alert.showAndWait();
	}

	public void setRootLayout(RootLayout rootLayout) {
		this.rootLayout = rootLayout;

		fileTable.setItems(rootLayout.getFileData());
	}

}