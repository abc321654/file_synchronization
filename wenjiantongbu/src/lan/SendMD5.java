package lan;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class SendMD5 {

    //    public static void main(String[] args) {
    private String host;

    public void setHost(String it) {
        host = it;
    }

    public boolean sendMain(String Path, int model) {
//        System.out.println("Debug");
//    	String host="172.16.85.206";			//服务端IP地址
        int modelport = 8888;
        int port = 33333;                            //端口号（第一次发送MD5.txt的端口号)
        int port2 = 55522;                            //端口号（第二次发送相应文件的端口号）
        int length = 0;
//        String Path="E:\\Testfile";   		//需要同步的文件夹
        String pathmd5 = "G:\\MD5.txt";//自定义md5文件存放路径
        byte[] sendByte = null;
        Socket socket = null;
        DataOutputStream dout = null;
        FileInputStream fin = null;
        DataInputStream In = null;
        int len = 0;
        boolean flag = true;
        try {
            try {
                System.out.println("Send!");

                socket = new Socket();
                socket.connect(new InetSocketAddress(host, port), 10 * 1000);
                dout = new DataOutputStream(socket.getOutputStream());

                File file = new File(Path);
                GetFileMD5Client GetMd5 = new GetFileMD5Client();
                GetMd5.setFile(file);
                GetMd5.setMD5(pathmd5);
                GetFileMD5Client.func(file);            //获取目标文件的MD5并形成txt文本
                File file2 = new File(pathmd5);
                fin = new FileInputStream(file2);
                sendByte = new byte[1024];
                /**
                 * three model:
                 * 1.full add
                 * 2.add
                 * 3.mirror
                 */
                if (model == 1)
                    dout.writeUTF(file.getName());//同步的文件名

                dout.writeUTF(Path);
                while ((length = fin.read(sendByte, 0, sendByte.length)) > 0) {
                    dout.write(sendByte, 0, length);
                    dout.flush();
                }
                socket.shutdownOutput();        //关闭socket的输出流，以防后面的接受出现问题
                byte[] bytes = new byte[1024];
                StringBuilder sb = new StringBuilder();
                //只有当客户端关闭它的输出流的时候，服务端才能取得结尾的-1
                In = new DataInputStream(socket.getInputStream());
                while ((len = In.read(bytes)) != -1) {
                    // 注意指定编码格式，发送方和接收方一定要统一，建议使用UTF-8
                    sb.append(new String(bytes, 0, len, "UTF-8"));
                }
                String strrr = "" + sb;
                String[] arr = strrr.split(",");        //arr数组存放的是需要发送的文件的路径
                //for(String st:arr)
                //System.out.println(st);
                Sendfile SEND = new Sendfile();
                SEND.setarr(arr);
                SEND.sethost(host);
                SEND.setport(port2);
                SEND.send();
                //调用SEND发送文件
            } catch (Exception e) {
                flag = false;
            } finally {
                if (dout != null)
                    dout.close();
                if (fin != null)
                    fin.close();
                if (In != null)
                    In.close();
                if (socket != null)
                    socket.close();

                deletxt.deleteFile(pathmd5);            //删除掉MD5.txt
            }
        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        }

        return flag;
    }

}