package lan;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Sendfile {
	String arr[]=null;	
	String host=null;
	int port;
	public void setarr(String arr[]) {
		this.arr=arr;
	}
	public void sethost(String host) {
		this.host=host;
	}
	public void setport(int port) {
		this.port=port;
	}
	public void send() throws IOException {
		int length = 0;
		byte[] sendByte = new byte[1024 * 1024];
		//String host = "172.16.85.206";		
		//int port = 55522;					
		Socket socket = null;
		DataOutputStream dout = null;
		FileInputStream fin = null;
		socket = new Socket();
		socket.connect(new InetSocketAddress(host, port), 10 * 1000);
		dout = new DataOutputStream(socket.getOutputStream());
		//先传送每个文件的字节长度给服务端
		for (int i = 1; i < arr.length; i++) {
			File file = new File(arr[i]);			
			dout.writeLong(file.length());
			dout.flush();
			System.out.println(file.length()+arr[i]);
		}
		//再按顺序传送文件
		for (int i = 1; i < arr.length; i++) {
			File file = new File(arr[i]);			
			long sum = 0;
			fin = new FileInputStream(file);
			while ((length = fin.read(sendByte)) != -1) {
				dout.write(sendByte, 0, length);
				// dout.flush();
				sum += length;
				System.out.println(sum + arr[i]);
			}

			// dout.flush();
		}
		if (dout != null)
			dout.close();
		if (fin != null)
			fin.close();
		if (socket != null)
			socket.close();
	}
}
