package lan;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class SendModel {
    private Socket socket;
    private DataInputStream In;
    private DataOutputStream dout;
    public void sendModel(String host, int modelport, int model) {
        try {
            socket = new Socket(host, modelport);
            In = new DataInputStream(socket.getInputStream());
            dout = new DataOutputStream(socket.getOutputStream());
            dout.write(model);
//                Thread.sleep(500);
            dout.flush();

            socket.close();
            In.close();
            dout.close();
        } catch (Exception e) {

        }
    }
}
