package lan;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

//获得文件或者文件夹内容的MD5码
public class GetFileMD5Server {
	File file;
	static String path_1=null;
	static String path_2=null;
	public void setFile(File file) {
		this.file=file;
	}
	/*public static void main(String[] args) throws IOException {
		String path = "C:\\Users\\hasee\\eclipse-workspace\\wenjian tongbu\\src\\tongbu"; // 要遍历的路径
		File file = new File(path); // 获取其file对象
		if (file.isDirectory()) { // 获得文件夹的MD5码
			func(file);
		}
	}
	*/
	public void setpath_2(String path_2) {
		GetFileMD5Server.path_2=path_2;
	}
	public static void func(File file) throws IOException {
		//String []str=new String [250];
//		String path = file.getAbsolutePath();
		if (file.isFile()) // 若是文件，直接打印
		{
			FileInputStream fis = new FileInputStream(file);
			String md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fis));
			IOUtils.closeQuietly(fis);
			String str = file+ "+MD5:" + md5;
			 System.out.println(str);
			 File file2 = new File(path_2); 
//			try {
				FileWriter outOne = new FileWriter(file2,true);
				BufferedWriter outTwo = new BufferedWriter(outOne);
				outTwo.write(str);
				outTwo.newLine();
				outTwo.close();
				outOne.close();
//			} catch (Exception e) {
//				System.out.println(e);
//			}
		}
		else {
		File[] fs = file.listFiles();// listFiles()方法是返回某个目录下所有文件和目录的绝对路径，返回的是File数组
		for (File f : fs) {
			if (f.isDirectory()) // 若是目录，则递归打印该目录下的文件
				func(f);
			if (f.isFile()) // 若是文件，直接打印
			{
				FileInputStream fis = new FileInputStream(f);
				String md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fis));
				IOUtils.closeQuietly(fis);
				String str = f+ "+MD5:" + md5;
				 System.out.println(str);
				 File file2 = new File(path_2); 
//				try {
					FileWriter outOne = new FileWriter(file2,true);
					BufferedWriter outTwo = new BufferedWriter(outOne);
					outTwo.write(str);
					outTwo.newLine();
					outTwo.close();
					outOne.close();
//				} catch (Exception e) {
//					System.out.println(e);
//				}
			}
		}
	}
}
}