package su_holsonWay;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

import su_fileSynchronizationSystem_Passerby.PathModel;

public class GetDifferentMD5 {

	public static void main(String[] args) throws IOException {
		String path_1 = "D:\\MD5.txt";// 传送过来的MD5的txt文本路径
		String path_2 = "D:\\MD5second.txt";// 保存同步文件的文件夹路径
		List<PathModel> listOfSave = new ArrayList<>();
		List<PathModel> listOfToSend = new ArrayList<>();
		List<PathModel> listOfNotSend = new ArrayList<>();
		listOfToSend = getListOfPathAndMd5(path_1);// 从txt获得想发送文件夹的MD5的list
		getTxtOfFileMD5(path_2);// 获得保存文件夹的MD5的txt文本
		String path2 = "保存文件夹的md5";// C:\Users\小苏\git\LAN-file-synchronization\保存文件夹的md5.txt
		listOfSave = getListOfPathAndMd5(path2);// 从txt获得保存文件夹的MD5的list
		 listOfToSend.removeAll(listOfSave);
//		listOfNotSend.addAll(compareFile(listOfToSend, listOfSave));
		System.out.println("这是能发送的文件的路径和md5");
		if (listOfToSend.size() == 0) {
			System.out.println("没有适合传送的文件");
		} else {
			for (PathModel pf : listOfToSend) {
				String sstr = pf.getPathAndMd5();
				System.out.println(sstr);
			}
		}
	}

	public static void getTxtOfFileMD5(String path) throws IOException {
		// 要遍历的路径
		File file = new File(path); // 获取其file对象
		if (file.isDirectory()) { // 获得文件夹的MD5码
			try {
				func(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (file.isFile()) { // 获得文件的MD5码
			FileInputStream fis = new FileInputStream(file);
			String md5;

			md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fis));

			IOUtils.closeQuietly(fis);
			String str = file + "+MD5:" + md5;
			String path2 = "保存文件夹的md5";// C:\Users\小苏\git\LAN-file-synchronization\保存文件夹的md5.txt
			File file2 = new File(path2);
			try {
				FileWriter outOne;

				outOne = new FileWriter(file2, false);

				BufferedWriter outTwo = new BufferedWriter(outOne);

				outTwo.write(str);

				outTwo.newLine();
				outTwo.close();
				outOne.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

	private static void func(File file, String path) throws IOException {
		// String path = file.getAbsolutePath();
		File[] fs = file.listFiles();// listFiles()方法是返回某个目录下所有文件和目录的绝对路径，返回的是File数组
		for (File f : fs) {
			if (f.isDirectory()) // 若是目录，则递归打印该目录下的文件
				func(f, path);
			if (f.isFile()) // 若是文件，直接打印
			{
				FileInputStream fis = new FileInputStream(f);
				String md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fis));
				IOUtils.closeQuietly(fis);
				String str = file + "+MD5:" + md5;
				String path2 = "保存文件夹的md5";// C:\Users\小苏\git\LAN-file-synchronization\保存文件夹的md5.txt
				File file2 = new File(path2);
				try {
					FileWriter outOne = new FileWriter(file2, true);
					BufferedWriter outTwo = new BufferedWriter(outOne);
					outTwo.write(str);
					outTwo.newLine();
					outTwo.close();
					outOne.close();
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		}
	}

	public static List<PathModel> getListOfPathAndMd5(String path) {

		// 获得存放文件的路径和MD5码的list
		List<PathModel> listOfPathAndMd5 = new ArrayList<>();
		File file = new File(path);
		try {
			Reader in = new FileReader(file);
			BufferedReader bufferRead = new BufferedReader(in);
			String str = null;
			while ((str = bufferRead.readLine()) != null) {
				String[] ss = str.split("\\+");
				PathModel pathModel = new PathModel(ss[0], ss[1]);
				listOfPathAndMd5.add(pathModel);
			}
			bufferRead.close();

		} catch (IOException e1) {
			System.out.println(e1.toString());
		}
		return listOfPathAndMd5;

	}

	public static List<PathModel> compareFile(List<PathModel> list1, List<PathModel> list2) {
		if (list2.size() == 0) {
			return list1;
		} else {
			for (PathModel pm : list1) {
				if (list2.contains(pm)) {
					list1.remove(pm);
				}
			}
			return list1;
		}

	}

}
