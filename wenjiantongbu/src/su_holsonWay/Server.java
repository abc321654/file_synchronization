package su_holsonWay;

import java.io.*;
import java.net.*;

public class Server {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		ServerSocket server = null;
		ServerThread thread;
		Socket you = null;
		while (true) {
			try {
				server = new ServerSocket(9080);
			} catch (IOException e) {
				System.out.println("正在监听");
			}
			try {
				System.out.println("等待客户呼叫");
				you = server.accept();
				System.out.println("客户的地址：" + you.getInetAddress());
			} catch (IOException e) {
				System.out.println("正在等待客户");
			}
			if (you != null) {
				new ServerThread(you).start();
			}
		}
	}
}

class ServerThread extends Thread {
	Socket socket;
	DataInputStream din = null;
	DataOutputStream dout = null;
	String s = null;

	ServerThread(Socket t) {
		socket = t;
		try {
			din = new DataInputStream(socket.getInputStream());
			dout = new DataOutputStream(socket.getOutputStream());
		} catch (IOException e) {
		}
	}

	public void run() {
		while (true) {
			try {
				String str = din.readUTF();
				dout.writeUTF(str);
			} catch (IOException e) {
				System.out.println("客户离开");
				return;
			}
		}
	}
}
