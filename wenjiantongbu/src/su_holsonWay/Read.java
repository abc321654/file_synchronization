package su_holsonWay;

import java.io.*;

public class Read implements Runnable  {
	DataInputStream in;
	
    public void setDataInputStream(DataInputStream in) {
    	this.in=in;
    }
    public  void run() {
    	String str=null;
    	while(true) {
    		 try {
    			 str=in.readUTF();
    			 System.out.println("获得的字符串是：");
    			 System.out.println(str);
    		 }catch(IOException e) {
    			 System.out.println("与服务器已断开"+e);
    			 break;
    		 }
    	}
    }
}
